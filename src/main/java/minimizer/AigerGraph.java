package minimizer;

import api.aiger_graph.Signal;
import minimizer.aiger_graph.*;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

public class AigerGraph implements api.aiger_graph.AigerGraph {

    private int[] metaData = new int[5];
    private List<Input> inputs = new Vector<>();
    private List<Output> outputs = new Vector<>();
    private List<Latch> latches = new Vector<>();
    private List<AndGate> andGates = new Vector<>();
    private List<InternalSignal> internalSignals = new Vector<>();
    private Hashtable<Integer, Signal> signalsByID = new Hashtable<>();
    private String comment;

    public AigerGraph(int maximumVariableIndex, int numberOfInputs, int numberOfLatches, int numberOfOutputs, int numberOfAndGates) {
        metaData[0]=maximumVariableIndex;
        metaData[1]=numberOfInputs;
        metaData[2]=numberOfLatches;
        metaData[3]=numberOfOutputs;
        metaData[4]=numberOfAndGates;
        checkMetaData();
        internalSignals.add(new ConstantSignal(0, false));
        internalSignals.add(new ConstantSignal(1, true));
        updateInternalSignalsByID();
    }

    private void checkMetaData() {
        for (int metaDatum : metaData) {
            if (metaDatum < 0) {
                throw new IllegalArgumentException("In the AIGER format, negative numbers are prohibited.");
            }
        }
        if( getMaximumVariableIndex() < getNumberOfInputs()+getNumberOfLatches()+getNumberOfAndGates() ){
            throw new IllegalArgumentException("Number of variable indexes have to be greater/equal than sum of number of inputs, outputs & And-Gates.");
        }
    }

    @Override
    public int getMaximumVariableIndex() {
        return metaData[0];
    }

    @Override
    public int getNumberOfInputs() {
        return metaData[1];
    }

    @Override
    public int getNumberOfLatches() {
        return metaData[2];
    }

    @Override
    public int getNumberOfOutputs() {
        return metaData[3];
    }

    @Override
    public int getNumberOfAndGates() {
        return metaData[4];
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public List<Input> getInputs() {
        return inputs;
    }

    @Override
    public List<Output> getOutputs() {
        return outputs;
    }

    @Override
    public List<Latch> getLatches() {
        return latches;
    }

    @Override
    public List<AndGate> getGates() {
        return andGates;
    }

    @Override
    public List<InternalSignal> getInternalSignals() {
        return internalSignals;
    }

    @Override
    public Signal getSignalByID(int id) {
        return signalsByID.get(id);
    }

    @Override
    public void updateInputsByID() {
        inputs.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
    }

    @Override
    public void updateOutputsByID() {
        outputs.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
    }

    @Override
    public void updateGatesByID() {
        andGates.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
    }

    @Override
    public void updateInternalSignalsByID() {
        internalSignals.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
    }

    @Override
    public void updateAllSignalByID() {
        inputs.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
        outputs.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
        andGates.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
        internalSignals.forEach(signal -> signalsByID.put(signal.getSignalID(), signal));
    }

    @Override
    public void clearSignalByID() {
        signalsByID.clear();
    }

    @Override
    public String getBooleanFunction() {
        StringBuilder builder = new StringBuilder("f(");
        if(!getInputs().isEmpty()) {
            getInputs().forEach(input -> builder.append(input.getName()).append(","));
            builder.deleteCharAt(builder.length()-1);
        }
        builder.append(") = ");
        if(outputs.isEmpty()) {
            builder.append("---GRAPH EMPTY---");
        } else {
            if (outputs.size() > 1) {
                builder.append("[");
                getOutputs().forEach(output -> builder.append(output.getName()).append(","));
                builder.deleteCharAt(builder.length() - 1);
                builder.append("] = [");
                getOutputs().forEach(output -> builder.append(output.getFunction()).append(","));
                builder.deleteCharAt(builder.length() - 1);
                builder.append("]");
            } else {
                getOutputs().forEach(output -> builder.append(output.getFunction()));
            }
        }
        return builder.toString();
    }
}
