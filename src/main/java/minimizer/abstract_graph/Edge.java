package minimizer.abstract_graph;

public class Edge implements api.abstract_graph.Edge {

    private Node[] nodes;
    private boolean inverted;

    public Edge(Node from, Node to, boolean inverted) {
        nodes = new Node[]{from, to};
        this.inverted = inverted;
    }

    @Override
    public Node getFrom() {
        return nodes[0];
    }

    @Override
    public Node getTo() {
        return nodes[1];
    }

    @Override
    public boolean isInverted() {
        return inverted;
    }

    @Override
    public String toString() {
        return nodes[0]+" -> "+nodes[1]+" (inverted: "+inverted+")";
    }
}
