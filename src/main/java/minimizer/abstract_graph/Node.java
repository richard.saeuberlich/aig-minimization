package minimizer.abstract_graph;

import minimizer.AbstractAigerGraph;

public class Node implements api.abstract_graph.Node{

    private final int id;
    private final Type type;
    private int depth;
    String name;

    public Node(int id, Type type, String name, int depth){
        this.id = id;
        this.type = type;
        this.name = name;
        this.depth = depth;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getDepth() {
        return depth;
    }

    @Override
    public void setDepth(int depth) {
        this.depth = depth;
    }

    /**
     * Ignore name &amp; depth for equality.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        } else if (obj == null || getClass() != obj.getClass() ){
            return false;
        } else {
            final Node other = (Node) obj;
            return this.id == other.id && this.type == other.type;
        }
    }

    /**
     * Ignore name &amp; depth for hashing.
     * This is needed for {@link java.util.Hashtable} in {@link AbstractAigerGraph}
     */
    @Override
    public int hashCode() {
        int hash = 41;
        hash = 59 * hash + id;
        hash = 59 * hash + type.ordinal();
        return hash;
    }

    @Override
    public String toString(){
        return (name.isEmpty()) ? Integer.toString(id) : name;
    }
}
