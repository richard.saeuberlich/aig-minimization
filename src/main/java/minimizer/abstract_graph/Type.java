package minimizer.abstract_graph;

public enum Type {

    INPUT, OUTPUT, GATE;

}
