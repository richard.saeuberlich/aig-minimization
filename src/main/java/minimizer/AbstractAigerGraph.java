package minimizer;

import api.abstract_graph.AbstractGraph;
import api.aiger_graph.GateSignal;
import api.aiger_graph.Name;
import api.aiger_graph.Signal;
import minimizer.abstract_graph.Edge;
import minimizer.abstract_graph.Node;
import minimizer.abstract_graph.Type;
import minimizer.aiger_graph.Output;

import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AbstractAigerGraph implements AbstractGraph {

    private Node output;
    private Hashtable<Integer, Node> inputs = new Hashtable<>();
    private Hashtable<Integer, Node> gates = new Hashtable<>();
    private Hashtable<Node, Hashtable<Node, Edge>> edges = new Hashtable<>();
    private Hashtable<Node, Hashtable<Node, Edge>> edgesReverse = new Hashtable<>();
    private Hashtable<Integer, Integer> depthWidth = new Hashtable<>();

    public AbstractAigerGraph(AigerGraph aigerGraph) {
        if(aigerGraph.getOutputs().size()>1) {
            throw new IllegalArgumentException("Aiger graph holds more than one output.");
        } else {
            if(!aigerGraph.getOutputs().isEmpty()) {
                build(aigerGraph, aigerGraph.getOutputs().get(0));
                buildReverseEdges();
            }
        }
    }

    @Override
    public Node getOutputNode(){
        return output;
    }

    @Override
    public List<Node> getInputNodes(){
        return inputs.values().stream().toList();
    }

    @Override
    public List<Node> getGateNodes(){
        return gates.values().stream().toList();
    }

    @Override
    public List<Node> getAllNodes(){
        return edges.keySet().stream().toList();
    }

    @Override
    public boolean isInputNode(Node node) {
        return inputs.get(node.getID()) != null;
    }

    @Override
    public Edge getEdge(Node from, Node to) {
        return edges.get(to).get(from);
    }

    @Override
    public List<Edge> getEdgesFrom(Node from) {
        return edgesReverse.get(from).values().stream().toList();
    }

    @Override
    public List<Edge> getEdgesTo(Node to) {
        return edges.get(to).values().stream().toList();
    }

    @Override
    public List<Edge> getAllEdges(){
        return edges.values().stream().flatMap(table -> table.values().stream()).collect(Collectors.toList());
    }

    @Override
    public int getDepth() {
        int depth = 0;
        for (Node input: inputs.values()){
            if(input.getDepth() > depth){
                depth = input.getDepth();
            }
        }
        return depth;
    }

    @Override
    public int getDepthWidth(int depth) {
        return depthWidth.get(depth);
    }

    /**
     * Output node is a special case since his ID appears two times. The output him self & the And-Gate that connects to the output.
     */
    private void build(AigerGraph aigerGraph, Output output) {
        this.output = new Node(0, Type.OUTPUT, output.getName(), -1);
        Node outputGate;
        if(output.isInverted()) {
            outputGate = new Node(output.getID()-1, Type.GATE, "", 0);
        } else {
            outputGate = new Node(output.getID(), Type.GATE, "", 0);
        }
        depthWidth.put(0,1);
        edges.put(this.output, new Hashtable<>());
        edges.get(this.output).put(outputGate, new Edge(this.output, outputGate, output.isInverted()));
        output.getPredecessor().forEach(predecessor -> addNodes(predecessor, 1));
        edges.keySet().forEach(node -> addEdges(node, null, aigerGraph));
        inputs.values().forEach(this::calculateDepth);
    }

    private void addNodes(Signal signal, int depth) {
        Node newNode;
        if(signal.isPrimary()) {
            newNode = new Node(signal.getID(), Type.INPUT, ((Name) signal).getName(), depth);
            edges.put(newNode, new Hashtable<>());
            inputs.put(newNode.getID(), newNode);
        } else {
            final int newDepth = ++depth;
            newNode = new Node(signal.getID(), Type.GATE, "", depth);
            edges.put(newNode, new Hashtable<>());
            gates.put(newNode.getID(), newNode);
            signal.getPredecessor().forEach(predecessor -> addNodes(predecessor, newDepth));
            //TODO: https://stackoverflow.com/questions/81346/most-efficient-way-to-increment-a-map-value-in-java
            if(depthWidth.get(depth)==null){
                depthWidth.put(depth, 1);
            } else {
                depthWidth.put(depth, depthWidth.get(depth)+1);
            }
        }
    }

    private void addEdges(Node current, Node previous, AigerGraph aigerGraph) {
        Signal currentSignal = aigerGraph.getSignalByID(current.getID());
        if (previous != null) {
            Signal previousSignal = aigerGraph.getSignalByID(previous.getID());
            if(previousSignal.isPrimary()) {
                edges.get(current).put(previous, new Edge(previous, current, previousSignal.isInverted()));
            } else {
                Optional<Boolean> inverted = ((GateSignal) previousSignal).isInvertedByID(current.getID());
                if(inverted.isPresent()){
                    edges.get(current).put(previous, new Edge(previous, current, inverted.get()));
                } else {
                    throw new IllegalArgumentException("Given node ID("+current.getID()+"), does NOT match with any of 2 And-Gate (ID: "+previous.getID()+") signals.");
                }
            }
        }
        if(current.getType()!=Type.INPUT) {
            currentSignal.getPredecessor().forEach(precedent -> {
                if(precedent.isPrimary()){
                    addEdges(inputs.get(precedent.getID()), current, aigerGraph);
                } else {
                    addEdges(gates.get(precedent.getID()), current, aigerGraph);
                }
            });
        }
    }

    private void calculateDepth(Node node) {
        int newDepth = 0;
        for (Node connectedNode: edges.get(node).keySet()){
            if(connectedNode.getDepth() > newDepth){
                newDepth = connectedNode.getDepth();
            }
        }
        node.setDepth(++newDepth);
    }

    private void buildReverseEdges() {
        edges.keySet().forEach(node -> {
            edgesReverse.put(node, new Hashtable<>());
        });
        edges.keySet().forEach(to -> {
            edges.get(to).keySet().forEach(from -> {
                edgesReverse.get(from).put(to, edges.get(to).get(from));
            });
        });
    }
}
