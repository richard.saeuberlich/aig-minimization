package minimizer.primitives;

public record PrimitiveInput(int signalID) { }
