package minimizer.primitives;

public record PrimitiveGate(int signalID_out, int signalID_lhs, int signalID_rhs) { }
