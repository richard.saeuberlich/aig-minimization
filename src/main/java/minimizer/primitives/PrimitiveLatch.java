package minimizer.primitives;

public record PrimitiveLatch(int signalID_current, int signalID_next) { }
