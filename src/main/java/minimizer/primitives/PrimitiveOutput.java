package minimizer.primitives;

public record PrimitiveOutput(int signalID) { }
