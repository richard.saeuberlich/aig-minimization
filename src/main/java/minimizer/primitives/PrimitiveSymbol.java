package minimizer.primitives;

public record PrimitiveSymbol(char type, int position, String name) { }
