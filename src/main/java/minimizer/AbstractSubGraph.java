package minimizer;

import minimizer.abstract_graph.Edge;
import minimizer.abstract_graph.Node;
import minimizer.abstract_graph.Type;

import java.util.Hashtable;
import java.util.List;

public class AbstractSubGraph {

    private boolean isExtended = false;
    private Hashtable<Node, Hashtable<Node, Edge>> edges = new Hashtable<>();

    public AbstractSubGraph(Node root, Node lhs, Node rhs, AbstractAigerGraph graph) {
        if(root == null || lhs == null || rhs == null) {
            throw new NullPointerException("Subgraph nodes mustn't be null.");
        } else {
            identifySubGraph(root, lhs, rhs, graph);
        }
    }

    public boolean isExtended() {
        return isExtended;
    }

    public int getNumberOfNodes() {
        return edges.keySet().size();
    }

    public int getNumberOfEdges() {
        return edges.values().stream().mapToInt(table -> table.values().size()).sum();
    }

    private void identifySubGraph(Node root, Node lhs, Node rhs, AbstractAigerGraph graph) {
        boolean lhs_is_input = graph.isInputNode(lhs);
        boolean rhs_is_input = graph.isInputNode(rhs);
        if(lhs_is_input) {
            if(rhs_is_input) {
                throw new IllegalStateException("Whoops, some shit happened here...");
            } else {
                extendLHS(root, lhs, rhs, graph);
            }
        } else {
            if(rhs_is_input) {
                extendRHS(root, lhs, rhs, graph);
            } else {
                noExtension(root, lhs, rhs, graph);
            }
        }
    }

    private void extendLHS(Node root, Node lhs, Node rhs, AbstractAigerGraph graph) {
        isExtended = true;
        //New nodes
        Node new_root = new Node(root.getID(), root.getType(), root.getName(), root.getDepth());
        Node new_lhs = new Node(root.getID()+rhs.getID()+lhs.getID(), Type.GATE, "", lhs.getDepth()-1);
        Node new_rhs = new Node(rhs.getID(), rhs.getType(), rhs.getName(), rhs.getDepth());

        //New edges
        boolean lhs_inverted = graph.getEdge(root, lhs).isInverted();
        edges.put(new_root, new Hashtable<>());
        edges.get(new_root).put(new_lhs, new Edge(new_root, new_lhs, lhs_inverted));
        edges.get(new_root).put(new_rhs, new Edge(new_root, new_rhs, graph.getEdge(root, rhs).isInverted()));
        edges.put(new_lhs, new Hashtable<>());
        edges.get(new_lhs).put(lhs, new Edge(new_lhs, lhs, lhs_inverted));
        edges.put(new_rhs, new Hashtable<>());
        edges.get(new_rhs).put(lhs, new Edge(new_rhs, lhs, lhs_inverted));
        //Old edges
        List<Edge> rhsEdges = graph.getEdgesFrom(rhs);
        edges.get(new_lhs).put(rhsEdges.get(0).getTo(), new Edge(new_lhs, rhsEdges.get(0).getTo(), rhsEdges.get(0).isInverted()));
        edges.get(new_rhs).put(rhsEdges.get(1).getTo(), new Edge(new_rhs, rhsEdges.get(1).getTo(), rhsEdges.get(1).isInverted()));
        graph.getEdgesTo(root).forEach(edge -> {
            edges.put(edge.getFrom(), new Hashtable<>());
            edges.get(edge.getFrom()).put(edge.getFrom(), new Edge(edge.getFrom(), new_root, edge.isInverted()));
        });
    }

    private void extendRHS(Node root, Node lhs, Node rhs, AbstractAigerGraph graph) {
        isExtended = true;
        //New nodes
        Node new_root = new Node(root.getID(), root.getType(), root.getName(), root.getDepth());
        Node new_lhs = new Node(lhs.getID(), lhs.getType(), lhs.getName(), lhs.getDepth());
        Node new_rhs = new Node(root.getID()+rhs.getID()+lhs.getID(), Type.GATE, "", rhs.getDepth()-1);

        //New edges
        boolean rhs_inverted = graph.getEdge(root, rhs).isInverted();
        edges.put(new_root, new Hashtable<>());
        edges.get(new_root).put(new_lhs, new Edge(new_root, new_lhs, graph.getEdge(root, lhs).isInverted()));
        edges.get(new_root).put(new_rhs, new Edge(new_root, new_rhs, rhs_inverted));
        edges.put(new_lhs, new Hashtable<>());
        edges.get(new_lhs).put(lhs, new Edge(new_lhs, rhs, rhs_inverted));
        edges.put(new_rhs, new Hashtable<>());
        edges.get(new_rhs).put(lhs, new Edge(new_rhs, rhs, rhs_inverted));
        //Old edges
        List<Edge> lhsEdges = graph.getEdgesFrom(lhs);
        edges.get(new_lhs).put(lhsEdges.get(0).getTo(), new Edge(new_lhs, lhsEdges.get(0).getTo(), lhsEdges.get(0).isInverted()));
        edges.get(new_rhs).put(lhsEdges.get(1).getTo(), new Edge(new_rhs, lhsEdges.get(1).getTo(), lhsEdges.get(1).isInverted()));
        graph.getEdgesTo(root).forEach(edge -> {
            edges.put(edge.getFrom(), new Hashtable<>());
            edges.get(edge.getFrom()).put(edge.getFrom(), new Edge(edge.getFrom(), new_root, edge.isInverted()));
        });
    }

    private void noExtension(Node root, Node lhs, Node rhs, AbstractAigerGraph graph) {
        //New nodes
        Node new_root = new Node(root.getID(), root.getType(), root.getName(), root.getDepth());
        Node new_lhs = new Node(lhs.getID(), lhs.getType(), lhs.getName(), lhs.getDepth());
        Node new_rhs = new Node(rhs.getID(), rhs.getType(), rhs.getName(), rhs.getDepth());

        //New edges
        edges.put(new_root, new Hashtable<>());
        edges.get(new_root).put(new_lhs, new Edge(new_root, new_lhs, graph.getEdge(root, lhs).isInverted()));
        edges.get(new_root).put(new_rhs, new Edge(new_root, new_rhs, graph.getEdge(root, rhs).isInverted()));
        //Old edges
        edges.put(new_lhs, new Hashtable<>());
        graph.getEdgesFrom(lhs).forEach(edge -> {
            edges.get(new_lhs).put(edge.getTo(), new Edge(new_lhs, edge.getTo(), edge.isInverted()));
        });
        edges.put(new_rhs, new Hashtable<>());
        graph.getEdgesFrom(rhs).forEach(edge -> {
            edges.get(new_rhs).put(edge.getTo(), new Edge(new_rhs, edge.getTo(), edge.isInverted()));
        });
        graph.getEdgesTo(root).forEach(edge -> {
            edges.put(edge.getFrom(), new Hashtable<>());
            edges.get(edge.getFrom()).put(edge.getFrom(), new Edge(edge.getFrom(), new_root, edge.isInverted()));
        });
    }

//    @Override
//    public String toString(){
//
//    }

}
