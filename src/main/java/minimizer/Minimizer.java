package minimizer;

import minimizer.abstract_graph.Edge;
import minimizer.abstract_graph.Node;

import java.util.List;
import java.util.Vector;

public class Minimizer {

    AbstractAigerGraph rootGraph;
    List<AbstractSubGraph> subGraphs;

    public Minimizer(AbstractAigerGraph graph) {
        rootGraph = graph;
    }

    public void start() {
        subGraphs = getSubGraphs(rootGraph);
        subGraphs.forEach(graph -> {
            System.out.println(graph+" -> "+graph.getNumberOfEdges());
            if(graph.getNumberOfEdges()!=7){
                System.out.println("FUCK");
            }
        });
    }

    private List<AbstractSubGraph> getSubGraphs(AbstractAigerGraph graph) {
        List<Node[]> subGraphsNodes = getSubGraphNodes(graph);
        List<AbstractSubGraph> result = new Vector<>();
        subGraphsNodes.forEach(array -> result.add(new AbstractSubGraph(array[0], array[1], array[2], graph)));
        return result;
    }

    private List<Node[]> getSubGraphNodes(AbstractAigerGraph graph) {
        List<Node[]> subGraphs = new Vector<>();
        graph.getGateNodes().forEach(gate -> {
            List<Edge> edges = graph.getEdgesFrom(gate);
            if(edges.size()>2){
                // TODO Hab 0 Ahnung wo hier der doppelte Knoten her kommt!?!
                return;
            }
            // Filter all gates that end in primary inputs on both sides
            if(!(graph.isInputNode(edges.get(0).getTo()) && graph.isInputNode(edges.get(1).getTo()))) {
                subGraphs.add(new Node[]{gate, edges.get(0).getTo(), edges.get(1).getTo()});
            }
        });
        return subGraphs;
    }
}
