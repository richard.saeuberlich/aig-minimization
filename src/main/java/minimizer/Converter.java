package minimizer;

import api.aiger_graph.Name;
import api.aiger_graph.Signal;
import minimizer.aiger_graph.*;
import minimizer.primitives.*;
import util.FileLoader;
import util.SignalSearchResult;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Converter {

    private final static String ascii_identifier = "aag";
    private int input_counter, output_counter, latch_counter;
    private int lastSizePrimitivePrimitiveLatches, lastSizePrimitivePrimitiveGates, lastSizePrimitivePrimitiveOutputs;

    public AigerGraph convertAigerFile(File file) {
        List<String> aigerFileLines = FileLoader.readFile(file);
        return convertAigerFile(aigerFileLines);
    }

    public AigerGraph convertAigerFile(List<String> aigerFileLines) {
        input_counter = 0;
        output_counter = 0;
        latch_counter = 0;
        return getGraph(aigerFileLines);
    }

    private AigerGraph getGraph(List<String> aigerFile) {
        List<List<String>> splittedLines = aigerFile.stream().map(line -> Arrays.asList(line.split(" "))).collect(Collectors.toList());
        AigerGraph graph;
        try {
            graph = getEmptyGraph(splittedLines.get(0));
        } catch (Exception exc) {
            throw new IllegalArgumentException("File does NOT follow AIGER format.", exc);
        }
        graph.setComment(getComment(splittedLines));
        return parseGraph(splittedLines, graph);
    }

    private AigerGraph parseGraph(List<List<String>> splittedLines, AigerGraph graph) {
        Hashtable<Integer, List<List<String>>> sortedLines = sortLines(splittedLines);
        Optional<Integer> max = sortedLines.keySet().stream().max(Integer::compare);
        List<PrimitiveInput> primitivePrimitiveInputs = new Vector<>();
        List<PrimitiveOutput> primitivePrimitiveOutputs = new LinkedList<>();
        List<PrimitiveLatch> primitivePrimitiveLatches = new LinkedList<>();
        List<PrimitiveGate> primitivePrimitiveGates = new LinkedList<>();
        List<PrimitiveSymbol> primitiveSymbols = new Vector<>();
        if(max.isPresent()) {
            for (int numberOfStrings=1; numberOfStrings<max.get()+1; numberOfStrings++) {
                switch (numberOfStrings) {
                    case 1 -> parseInputsOutputs(Optional.ofNullable(sortedLines.get(numberOfStrings)), primitivePrimitiveInputs, primitivePrimitiveOutputs, graph);
                    case 2 -> parseSymbolsLatches(Optional.ofNullable(sortedLines.get(numberOfStrings)), primitivePrimitiveLatches, primitiveSymbols, graph.getNumberOfLatches());
                    case 3 -> parseGates(Optional.ofNullable(sortedLines.get(numberOfStrings)), primitivePrimitiveGates, graph.getNumberOfAndGates());
                    default -> throw new IllegalArgumentException("File does NOT follow AIGER format.\nExcept for header & comment, max entries per line is: 3");
                }
            }
            if (primitivePrimitiveInputs.isEmpty() && primitivePrimitiveOutputs.isEmpty() && primitivePrimitiveLatches.isEmpty() && !primitivePrimitiveGates.isEmpty()) {
                throw new IllegalArgumentException("File does NOT follow AIGER format.\n And-Gates without any input doesn't make any sense");
            } else {
                return populateGraph(primitivePrimitiveInputs, primitivePrimitiveOutputs, primitivePrimitiveLatches, primitivePrimitiveGates, primitiveSymbols, graph);
            }
        } else {
            return graph;
        }
    }

    private void parseInputsOutputs(Optional<List<List<String>>> lines, List<PrimitiveInput> inputs, List<PrimitiveOutput> outputs, AigerGraph graph) {
        if(lines.isPresent()){
            int numOfInputs = graph.getNumberOfInputs();
            int numOfOutputs = graph.getNumberOfOutputs();
            if(numOfInputs+numOfOutputs != lines.get().size()) {
                throw new IllegalArgumentException("File does NOT follow AIGER format.\nNumber of inputs & outputs in header does NOT match with that in body.");
            } else {
                for(int i=0; i<numOfInputs; i++) {
                    inputs.add(new PrimitiveInput(Integer.parseInt(lines.get().get(i).get(0))));
                }
                for(int i=numOfInputs; i<numOfInputs+numOfOutputs; i++) {
                    outputs.add(new PrimitiveOutput(Integer.parseInt(lines.get().get(i).get(0))));
                }
            }
        }
    }

    private void parseSymbolsLatches(Optional<List<List<String>>> lines, List<PrimitiveLatch> primitivePrimitiveLatches, List<PrimitiveSymbol> primitiveSymbols, int numberOfLatches) {
        AtomicInteger latchCounter = new AtomicInteger();
        lines.ifPresent(lists -> lists.forEach(line -> {
            char type = line.get(0).charAt(0);
            if (Character.isDigit(type)) {
                primitivePrimitiveLatches.add(new PrimitiveLatch(Integer.parseInt(line.get(0)), Integer.parseInt(line.get(1))));
                latchCounter.getAndIncrement();
            } else {
                primitiveSymbols.add(new PrimitiveSymbol(type, Integer.parseInt(line.get(0).substring(1)), line.get(1)));
            }
        }));
        if(latchCounter.get() != numberOfLatches){
            throw new IllegalArgumentException("File does NOT follow AIGER format.\nNumber of latches in header does NOT match with that in body.");
        }
    }

    private void parseGates(Optional<List<List<String>>> lines, List<PrimitiveGate> primitivePrimitiveGates, int numberOfAndGates) {
        AtomicInteger gateCounter = new AtomicInteger();
        lines.ifPresent(lists -> lists.forEach(line -> {
            primitivePrimitiveGates.add(new PrimitiveGate(Integer.parseInt(line.get(0)), Integer.parseInt(line.get(1)), Integer.parseInt(line.get(2))));
            gateCounter.getAndIncrement();
        }));
        if(gateCounter.get() != numberOfAndGates){
            throw new IllegalArgumentException("File does NOT follow AIGER format.\nNumber of 'And-Gates' in header does NOT match with that in body.");
        }
    }

    private AigerGraph populateGraph(List<PrimitiveInput> primitivePrimitiveInputs, List<PrimitiveOutput> primitivePrimitiveOutputs, List<PrimitiveLatch> primitivePrimitiveLatches, List<PrimitiveGate> primitivePrimitiveGates, List<PrimitiveSymbol> primitiveSymbols, AigerGraph graph) {
        addInputs(primitivePrimitiveInputs, graph);
        graph.updateInputsByID();
        lastSizePrimitivePrimitiveLatches = primitivePrimitiveLatches.size();
        lastSizePrimitivePrimitiveGates = primitivePrimitiveGates.size();
        lastSizePrimitivePrimitiveOutputs = primitivePrimitiveOutputs.size();
        populateIterative(primitivePrimitiveOutputs, primitivePrimitiveLatches, primitivePrimitiveGates, graph);
        setSymbols(primitiveSymbols, graph);
        return graph;
    }

    private void addInputs(List<PrimitiveInput> primitivePrimitiveInputs, AigerGraph graph) {
        primitivePrimitiveInputs.forEach(input -> graph.getInputs().add(new Input(input.signalID(), input_counter++)));
    }

        /**
         * ATTENTION: Keep the order
         *      1) addLatches
         *      2) addOutputs
         *      3) addGates
         */
    private void populateIterative(List<PrimitiveOutput> primitivePrimitiveOutputs, List<PrimitiveLatch> primitivePrimitiveLatches, List<PrimitiveGate> primitivePrimitiveGates, AigerGraph graph) {
        addLatches(primitivePrimitiveLatches, graph);
        addOutputs(primitivePrimitiveOutputs, graph);
        addGates(primitivePrimitiveGates, graph);
        if(!finished(primitivePrimitiveOutputs, primitivePrimitiveLatches, primitivePrimitiveGates)){
            if (lastSizePrimitivePrimitiveLatches > primitivePrimitiveLatches.size() || lastSizePrimitivePrimitiveGates > primitivePrimitiveGates.size() || lastSizePrimitivePrimitiveOutputs > primitivePrimitiveOutputs.size()) {
                lastSizePrimitivePrimitiveLatches = primitivePrimitiveLatches.size();
                lastSizePrimitivePrimitiveOutputs = primitivePrimitiveOutputs.size();
                lastSizePrimitivePrimitiveGates = primitivePrimitiveGates.size();
            } else {
                throw new IllegalStateException("Size of signals didn't change since last iteration, therefore this process would repeat endlessly.\n\tCheck AIGER file for failures");
            }
            populateIterative(primitivePrimitiveOutputs, primitivePrimitiveLatches, primitivePrimitiveGates, graph);
        }
    }

    /**
     * <pre>
     * How to distinguish even & odd numbers:
     *     1 = 0001
     *    12 = 1100 (even)
     *    13 = 1101 (odd)
     *
     *      1100               1101
     *    & 0001               0001
     *   --------           --------
     *      0000 = 0 (even)    0001 = 1 (odd)
     * <pre>
     */
    private SignalSearchResult searchSignal(int signalID, AigerGraph graph) {
        Signal signal = graph.getSignalByID(signalID);
        if(signal == null) {
            if ((signalID & 1) > 0) {
                //odd
                return new SignalSearchResult(graph.getSignalByID(signalID-1), true);
            } else {
                //even
                return new SignalSearchResult(graph.getSignalByID(signalID+1), true);
            }
        } else {
            return new SignalSearchResult(signal, false);
        }
    }

    private void addOutputs(List<PrimitiveOutput> primitivePrimitiveOutputs, AigerGraph graph) {
        PrimitiveOutput output;
        SignalSearchResult searchResult;
        for(Iterator<PrimitiveOutput> it = primitivePrimitiveOutputs.iterator(); it.hasNext(); ) {
            output = it.next();
            searchResult = searchSignal(output.signalID(), graph);
            if(searchResult.signal() != null) {
                graph.getOutputs().add(new Output(output.signalID(), searchResult.signal(), searchResult.inverted(), output_counter++));
                it.remove();
            }
        }
        graph.updateOutputsByID();
    }

    private void addLatches(List<PrimitiveLatch> primitivePrimitiveLatches, AigerGraph graph) {
        PrimitiveLatch latch;
        SignalSearchResult currentSignal, nextSignal;
        for(Iterator<PrimitiveLatch> it = primitivePrimitiveLatches.iterator(); it.hasNext(); ){
            latch = it.next();
            currentSignal = searchSignal(latch.signalID_current(), graph);
            if(currentSignal.signal() == null) {
                currentSignal = addNewInternalSignal(latch.signalID_current(), graph);
            }
            nextSignal = searchSignal(latch.signalID_next(), graph);
            if(nextSignal.signal() != null) {
                graph.getLatches().add(new Latch(currentSignal.signal(), nextSignal.signal(), currentSignal.inverted(), nextSignal.inverted(), latch_counter++));
                it.remove();
            }
        }
        graph.updateInternalSignalsByID();
    }

    private SignalSearchResult addNewInternalSignal(int signalID, AigerGraph graph) {
        InternalSignal newSignal = new InternalSignal(signalID);
        graph.getInternalSignals().add(newSignal);
        graph.updateInternalSignalsByID();
        return new SignalSearchResult(newSignal, false);
    }

    private void addGates(List<PrimitiveGate> primitivePrimitiveGates, AigerGraph graph) {
        PrimitiveGate gate;
        SignalSearchResult lhs, rhs;
        for(Iterator<PrimitiveGate> it = primitivePrimitiveGates.iterator(); it.hasNext(); ){
            gate = it.next();
            lhs = searchSignal(gate.signalID_lhs(), graph);
            if(lhs.signal() != null) {
                rhs = searchSignal(gate.signalID_rhs(), graph);
                if(rhs.signal() != null) {
                    graph.getGates().add(new AndGate(gate.signalID_out(), lhs.signal(), rhs.signal(), lhs.inverted(), rhs.inverted()));
                    it.remove();
                }
            }
        }
        graph.updateGatesByID();
    }

    private boolean finished(List<PrimitiveOutput> primitivePrimitiveOutputs, List<PrimitiveLatch> primitivePrimitiveLatches, List<PrimitiveGate> primitivePrimitiveGates) {
        return primitivePrimitiveOutputs.isEmpty() && primitivePrimitiveLatches.isEmpty() && primitivePrimitiveGates.isEmpty();
    }

    private static void setSymbols(List<PrimitiveSymbol> primitiveSymbols, AigerGraph graph) {
        primitiveSymbols.forEach(primitiveSymbol -> {
            Name signal = graph.getInputs().get(primitiveSymbol.position());
            if(signal !=  null) {
                switch (primitiveSymbol.type()) {
                    case 'i' -> signal.setName(primitiveSymbol.name());
                    case 'o' -> signal.setName(primitiveSymbol.name());
                    case 'l' -> signal.setName(primitiveSymbol.name());
                    default -> throw new IllegalArgumentException("File does NOT follow AIGER format.\nFor symbols only following identifiers are allowed: 'i', 'o', 'l'");
                }
            }
        });
    }

    /**
     * Parses for comment of AIGER file. If there is any, it is removed from the file/lines and returned as single string.
     *
     * @param splittedLines List of list of strings. Every list represents one line of an aiger file.
     * @return Comment as single String. If there is none, an empty string is returned.
     */
    private String getComment(List<List<String>> splittedLines) {
        int startOfComment = 0;
        for(int i=0; i<splittedLines.size(); i++) {
            if(splittedLines.get(i).size()==1 && splittedLines.get(i).get(0).equalsIgnoreCase("c")){
                startOfComment = i;
                break;
            }
        }
        if(startOfComment > 0) {
            StringBuilder builder = new StringBuilder();
            for(int i=startOfComment+1; i<splittedLines.size(); i++) {
                splittedLines.get(i).forEach(word -> builder.append(word).append(" "));
                builder.append(System.lineSeparator());
            }
            if (splittedLines.size() > startOfComment + 1) {
                splittedLines.subList(startOfComment + 1, splittedLines.size()).clear();
            }
            splittedLines.remove(startOfComment);
            return builder.toString();
        } else {
            return "";
        }
    }

    private Hashtable<Integer, List<List<String>>> sortLines(List<List<String>> splittedLines) {
        Hashtable<Integer, List<List<String>>> result = new Hashtable<>();
        List<String> line;
        for(int i=1; i<splittedLines.size(); i++){
            line = splittedLines.get(i);
            List<List<String>> mapping = result.get(line.size());
            if(mapping == null){
                result.put(line.size(), new Vector<>());
                result.get(line.size()).add(line);
            } else {
                mapping.add(line);
            }
        }
        return result;
    }

    private AigerGraph getEmptyGraph(List<String> firstLine) {
        if(firstLine.size() != 6){
           throw new IllegalArgumentException("Header must hold 6 entries.");
        } else {
            if(!firstLine.get(0).equalsIgnoreCase(ascii_identifier)){
                throw new IllegalArgumentException("File is NOT ASCII format. Identifier: "+firstLine.get(0));
            } else {
                int[] meta = new int[5];
                for(int i=1; i<firstLine.size(); i++){
                    meta[i-1] = Integer.parseInt(firstLine.get(i));
                }
                return new AigerGraph(meta[0], meta[1], meta[2], meta[3], meta[4]);
            }
        }
    }
}
