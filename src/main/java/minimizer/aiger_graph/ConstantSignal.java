package minimizer.aiger_graph;

public final class ConstantSignal extends InternalSignal {

    private final boolean signal;

    public ConstantSignal(int signal_id, boolean signal) {
        super(signal_id);
        this.signal = signal;
    }

    @Override
    public boolean getSignal() {
        return signal;
    }

    @Override
    public String getFunction() {
        return (signal) ? "1" : "0";
    }
}
