package minimizer.aiger_graph;

import api.aiger_graph.Signal;

/**
 * <pre>
 * lhs = Left-hand-side
 * rhs = Right-hand-side
 * out = Output
 *
 *        lhs    rhs
 *         |      |
 *      --------------
 *      |            |
 *      |     OR     |
 *      |            |
 *      --------------
 *            |
 *           out
 * </pre>
 */
public class OrGate extends AndGate {

    public final static String gateSymbol = "+";
    private final static String separator1 = " "+ gateSymbol +" !";
    private final static String separator2 = " "+ gateSymbol +" ";
    private final static String separator3 = " "+ gateSymbol +" !";
    private final static String separator4 = " "+ gateSymbol +" ";

    public OrGate(int signal_id, Signal lhs, Signal rhs, boolean invert_lhs, boolean invert_rhs){
        super(signal_id, lhs, rhs, invert_lhs, invert_rhs);
    }

    public OrGate(int signal_id, Signal lhs, Signal rhs){
        this(signal_id, lhs, rhs, false, false);
    }

    @Override
    public boolean getSignal() {
        if(invert_lhs) {
            if(invert_rhs) {
                return !lhs.getSignal() | !rhs.getSignal();
            } else {
                return !lhs.getSignal() | rhs.getSignal();
            }
        } else {
            if(invert_rhs) {
                return lhs.getSignal() | !rhs.getSignal();
            } else {
                return lhs.getSignal() | rhs.getSignal();
            }
        }
    }
}
