package minimizer.aiger_graph;

import api.aiger_graph.Name;
import api.aiger_graph.Signal;

/**
 * <pre>
 *                 -------------
 *                 |           |
 *    current(X) --|           |--  next(X)
 *                 |           |
 *                 -------------
 *  </pre>
 */
public class Latch extends Output implements Name {

    private final static String namePrefix = "L";
    private Signal next;
    private boolean invertNext;

    public Latch(Signal current, Signal next, String name, boolean invertCurrent, boolean invertNext) {
        super(-1, current, invertCurrent, name); // Latch has no ID !!!
        this.next = next;
        this.invertNext = invertNext;
    }

    public Latch(Signal current, Signal next, boolean invertCurrent, boolean invertNext, int labelID) {
        this(current, next, namePrefix+labelID, invertCurrent, invertNext);
    }

    public Latch(Signal current, Signal next, String name) {
        this(current, next, name, false, false);
    }

    public Latch(Signal current, Signal next, int labelID) {
        this(current, next, namePrefix+labelID);
    }

    private void updateSignals() {
        signal = next;
        //next = ???;
    }

    @Override
    public String getFunction(){
        return signal.getFunction();
    }

    @Override
    public boolean isPrimary() {
        return false;
    }

}
