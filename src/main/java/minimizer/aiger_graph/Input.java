package minimizer.aiger_graph;

import api.aiger_graph.Name;

/**
 * <pre>
 *
 *      X ---------- ?
 *
 * </pre>
 *
 * A signal source, which is non inverted all the time.
 *
 */
public class Input extends InternalSignal implements Name {

    private final static String namePrefix = "x";
    protected String name;

    public Input(int signal_id, String name) {
        super(signal_id);
        this.name = name;
    }

    public Input(int signal_id, int labelID) {
        this(signal_id, namePrefix+labelID);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getFunction(){
        return getName();
    }

    @Override
    public boolean isPrimary() {
        return true;
    }
}
