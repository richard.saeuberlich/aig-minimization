package minimizer.aiger_graph;

import api.aiger_graph.InvertSignal;
import api.aiger_graph.Name;
import api.aiger_graph.Signal;

import java.util.List;
import java.util.Vector;

/**
 * <pre>
 *
 *      ? ---------- Y
 *
 * </pre>
 *
 * A signal sink, which can be inverted or not.
 *
 */
public class Output extends Input implements Name, InvertSignal {

    private final static String namePrefix = "y";
    protected Signal signal;
    protected boolean invertSignal;

    public Output(int signal_id, Signal signal, boolean invertSignal, String name) {
        super(signal_id, name);
        this.signal = signal;
        this.invertSignal = invertSignal;
        this.name = name;
    }

    public Output(int signal_id, Signal signal, boolean invertSignal, int labelID) {
        this(signal_id, signal, invertSignal, namePrefix+labelID);
    }

    public Output(int signal_id, Signal signal, String name) {
        this(signal_id, signal, false, name);
    }

    public Output(int signal_id, Signal signal, int labelID) {
        this(signal_id, signal, namePrefix+labelID);
    }

    @Override
    public boolean isInverted() {
        return invertSignal;
    }

    @Override
    public boolean getSignal() {
        return invertSignal != signal.getSignal();
    }

    @Override
    public List<Signal> getPredecessor() {
        Vector<Signal> result = new Vector<>();
        result.add(signal);
        return result;
    }

    @Override
    public String getFunction() {
        if(invertSignal) {
            return "!" + signal.getFunction();
        } else {
            return signal.getFunction();
        }
    }

    @Override
    public void invertSignal() {
        invertSignal = !invertSignal;
    }
}
