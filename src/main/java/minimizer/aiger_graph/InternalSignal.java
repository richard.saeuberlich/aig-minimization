package minimizer.aiger_graph;

import api.aiger_graph.ID;
import api.aiger_graph.Signal;

import java.util.List;
import java.util.Vector;

public class InternalSignal implements Signal, ID {

    protected final int signal_id;

    public InternalSignal(int signal_id) {
        this.signal_id = signal_id;
    }

    @Override
    public int getSignalID() {
        return signal_id;
    }

    @Override
    public boolean getSignal() {
        return true;
    }

    @Override
    public boolean isInverted() {
        return false;
    }

    @Override
    public boolean isPrimary() {
        return false;
    }

    @Override
    public List<Signal> getPredecessor() {
        return new Vector<>();
    }

    @Override
    public int getID() {
        return signal_id;
    }

    @Override
    public String getFunction() {
        return Integer.toString(signal_id);
    }

    @Override
    public String toString() {
        return "Signal ID: "+signal_id;
    }
}
