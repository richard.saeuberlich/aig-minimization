package minimizer.aiger_graph;

import api.aiger_graph.ID;
import api.aiger_graph.GateSignal;
import api.aiger_graph.Signal;

import java.util.List;
import java.util.Optional;
import java.util.Vector;

/**
 * <pre>
 * lhs = Left-hand-side
 * rhs = Right-hand-side
 * out = Output
 *
 *        lhs   rhs
 *         |     |
 *      -------------
 *      |           |
 *      |    AND    |
 *      |           |
 *      -------------
 *            |
 *           out
 * </pre>
 */
public class AndGate extends InternalSignal implements ID, GateSignal {

    public final static String gateSymbol = "*";
    private final static String start1 = "( !";
    private final static String start2 = "( ";
    private final static String separator1 = " "+ gateSymbol +" !";
    private final static String separator2 = " "+ gateSymbol +" ";
    private final static String ending = " )";

    protected Signal lhs, rhs;
    protected boolean invert_lhs, invert_rhs;

    public AndGate(int signal_id, Signal lhs, Signal rhs, boolean invert_lhs, boolean invert_rhs){
        super(signal_id);
        this.lhs = lhs;
        this.rhs = rhs;
        this.invert_lhs = invert_lhs;
        this.invert_rhs = invert_rhs;
    }

    public AndGate(int signal_id, Signal lhs, Signal rhs){
        this(signal_id, lhs, rhs, false, false);
    }

    @Override
    public boolean getSignal() {
        if(invert_lhs) {
            if(invert_rhs) {
                return !lhs.getSignal() & !rhs.getSignal();
            } else {
                return !lhs.getSignal() & rhs.getSignal();
            }
        } else {
            if(invert_rhs) {
                return lhs.getSignal() & !rhs.getSignal();
            } else {
                return lhs.getSignal() & rhs.getSignal();
            }
        }
    }

    @Override
    public List<Signal> getPredecessor() {
        Vector<Signal> result = new Vector<>();
        result.add(lhs);
        result.add(rhs);
        return result;
    }

    @Override
    public String getFunction(){
        if(invert_lhs) {
            if(invert_rhs) {
                return start1+lhs.getFunction()+separator1+rhs.getFunction()+ending;
            } else {
                return start1+lhs.getFunction()+separator2+rhs.getFunction()+ending;
            }
        } else {
            if(invert_rhs) {
                return start2+lhs.getFunction()+separator1+rhs.getFunction()+ending;
            } else {
                return start2+lhs.getFunction()+separator2+rhs.getFunction()+ending;
            }
        }
    }

    @Override
    public Signal get_lhs() {
        return lhs;
    }

    @Override
    public Signal get_rhs() {
        return rhs;
    }

    @Override
    public Optional<Signal> getSignalByID(int id) {
        if(lhs.getID()==id){
            return Optional.of(lhs);
        } else if(rhs.getID()==id){
            return Optional.of(rhs);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean is_lhs_inverted() {
        return invert_lhs;
    }

    @Override
    public boolean is_rhs_inverted() {
        return invert_rhs;
    }

    @Override
    public Optional<Boolean> isInvertedByID(int id) {
        if(lhs.getID()==id){
            return Optional.of(invert_lhs);
        } else if(rhs.getID()==id){
            return Optional.of(invert_rhs);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void invertLHS() {
        invert_lhs = !invert_lhs;
    }

    @Override
    public void invertRHS() {
        invert_rhs = !invert_rhs;
    }
}
