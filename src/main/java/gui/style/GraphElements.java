package gui.style;

/**
 * https://graphstream-project.org/doc/Tutorials/Graph-Visualisation/
 *
 * https://graphstream-project.org/doc/Advanced-Concepts/GraphStream-CSS-Reference/
 */
public enum GraphElements {

    GRAPH("graph"),
    NODE("node"),
    EDGE("edge"),
    SPRITE("sprite");

    private final String element;

    GraphElements(String element){
        this.element = element;
    }

    public String get() {
        return element;
    }
}
