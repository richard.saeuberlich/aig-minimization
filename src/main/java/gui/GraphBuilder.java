package gui;

import api.abstract_graph.AbstractGraph;
import javafx.scene.Scene;
import minimizer.abstract_graph.Type;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.fx_viewer.FxViewPanel;
import org.graphstream.ui.fx_viewer.FxViewer;
import org.graphstream.ui.javafx.FxGraphRenderer;
import org.graphstream.ui.spriteManager.Sprite;
import org.graphstream.ui.spriteManager.SpriteManager;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GraphBuilder {

    // Screen size = 800 x 600 pixels
    private final int[] initialScreenSize = new int []{800, 600};

    public static String graphStyle() {
        return "graph {" +
                "fill-color: white;" +
                "}";
    }

    public static String nodeStyle() {
        return "node {" +
                "text-alignment: at-right;" +
                "text-color: black;" +
                "text-style: bold;" +
                "size: 15px, 20px;" +
                "fill-mode: plain;" +
                "fill-color: grey;" +
                "}";
    }

    public static String inputStyle() {
        return "node.input {" +
                "text-alignment: center;" +
                "fill-color: yellow;" +
                "}";
    }

    public static String outputStyle() {
        return "node.output {" +
                "text-alignment: center;" +
                "fill-color: green;" +
                "}";
    }

    public static String gateStyle() {
        return "node.gate {" +
                "text-color: white;" +
                "text-background-mode: rounded-box;" +
                "text-background-color: #222C;" +
                "text-padding: 1px;" +
                "text-offset: 5px, 3px;" +
                "stroke-mode: plain;" +
                "stroke-color: black;" +
                "}";
    }

    public static String edgeStyle() {
        return "edge {" +
                "arrow-shape: arrow;" +
                "}";
    }

    public static String spriteStyle() {
        return "sprite.invertedEdge {" +
                "shape: circle;" +
                "fill-color: black;" +
                "size: 10px;" +
                "}";
    }

    private Hashtable<minimizer.abstract_graph.Node, Integer> calculatePositions(AbstractGraph abstractGraph, boolean hierarchical) {
        if(hierarchical){
            Hashtable<minimizer.abstract_graph.Node, Integer> result = new Hashtable<>(abstractGraph.getDepth());

            Hashtable<Integer, List<minimizer.abstract_graph.Node>> depthTable = getDepthTable(abstractGraph);

            int max_x = getMaxWidth(depthTable);

            List<List<minimizer.abstract_graph.Node>> positions = new Vector<>(2);
            positions.add(new Vector<>(max_x)); //gate_left = index: 0
            positions.add(new Vector<>(max_x)); //gate_right = index: 1
            positions.add(new Vector<>(max_x)); //input = index: 2

            minimizer.abstract_graph.Node firstGate = abstractGraph.getEdgesTo(abstractGraph.getOutputNode()).get(0).getTo();
            List<minimizer.abstract_graph.Node> prescedents = abstractGraph.getEdgesFrom(firstGate).stream().map(minimizer.abstract_graph.Edge::getTo).collect(Collectors.toList());
            prescedents.remove(firstGate);
            if(holdsPrimary(prescedents)){
                positions.get(0).add(firstGate);
            } else {
                positions.get(1).add(firstGate);
            }
            prescedents.forEach(nextNode -> fillPositions(nextNode, positions, abstractGraph));

            AtomicInteger x_position = new AtomicInteger(0);
            positions.get(0).forEach(node -> result.put(node, x_position.incrementAndGet()));
            positions.get(1).forEach(node -> result.put(node, x_position.incrementAndGet()));
            int stepLength = (max_x/positions.get(2).size())+1;
            int inputPosition = 0;
            for (minimizer.abstract_graph.Node input : positions.get(2)) {
                result.put(input, inputPosition);
                inputPosition += stepLength;
            }
            return result;
        } else {
            return null;
        }
    }

    private void fillPositions(minimizer.abstract_graph.Node node, List<List<minimizer.abstract_graph.Node>> positions, AbstractGraph abstractGraph){
        if(node.getType()==Type.GATE){
            List<minimizer.abstract_graph.Node> prescedents = abstractGraph.getEdgesFrom(node).stream().map(minimizer.abstract_graph.Edge::getTo).collect(Collectors.toList());
            if(holdsPrimary(prescedents)){
                if(!positions.get(0).contains(node)) {
                    positions.get(0).add(node);
                }
            } else {
                if(!positions.get(1).contains(node)) {
                    positions.get(1).add(node);
                }
            }
            prescedents.forEach(nextNode -> fillPositions(nextNode, positions, abstractGraph));
        } else {
            if(!positions.get(2).contains(node)){
                positions.get(2).add(node);
            };
        }
    }

    private boolean holdsPrimary(List<minimizer.abstract_graph.Node> prescedents) {
        return prescedents.stream().anyMatch(node -> node.getType()==Type.INPUT);
    }

    Hashtable<Integer, List<minimizer.abstract_graph.Node>> getDepthTable(AbstractGraph abstractGraph){
        Hashtable<Integer, List<minimizer.abstract_graph.Node>> result = new Hashtable<>();
        abstractGraph.getAllNodes().forEach(node -> {
            if(result.get(node.getDepth())==null){
                List<minimizer.abstract_graph.Node> newDepth = new Vector<>();
                newDepth.add(node);
                result.put(node.getDepth(), newDepth);
            } else {
                result.get(node.getDepth()).add(node);
            }
        });
        return result;
    }

    int getMaxWidth(Hashtable<Integer, List<minimizer.abstract_graph.Node>> depthTable) {
        int max_x = 0;
        for(List<minimizer.abstract_graph.Node> list: depthTable.values()){
            if(max_x<list.size()){
                max_x = list.size();
            }
        }
        return max_x;
    }

    public Graph build(AbstractGraph abstractGraph, boolean swingUI, boolean hierarchical) {
        Graph graph = new SingleGraph("AIGER graph");

        SpriteManager spriteManager = new SpriteManager(graph);

        Hashtable<minimizer.abstract_graph.Node, Integer> x_positions = calculatePositions(abstractGraph, hierarchical);

        int max_x_position = 0;
        if(hierarchical) {
            for (Integer y_position : x_positions.values()) {
                if (max_x_position < y_position) {
                    max_x_position = y_position;
                }
            }
        }

        int middle = max_x_position/2;
        int max_y_position = abstractGraph.getDepth();
        abstractGraph.getAllNodes().forEach(node -> {
            Node graphNode = graph.addNode(node.toString());
            graphNode.setAttribute("ui.label", node.toString());
            switch (node.getType()) {
                case INPUT -> {
                    graphNode.setAttribute("ui.class", "input");
                    if(hierarchical) {
                        graphNode.setAttribute("xyz", x_positions.get(node), 0, 0);
                    }
                }
                case OUTPUT -> {
                    graphNode.setAttribute("ui.class", "output");
                    if(hierarchical) {
                        graphNode.setAttribute("xyz", 1, max_y_position, 0);
                    }
                }
                case GATE -> {
                    graphNode.setAttribute("ui.class", "gate");
                    if(hierarchical) {
                        graphNode.setAttribute("xyz", x_positions.get(node), node.getDepth(), 0);
                    }
                }
            }
            //System.out.println(node+" -> x:"+x_positions.get(node));
        });

        abstractGraph.getAllEdges().forEach(edge -> {
            Edge graphEdge = graph.addEdge(edge.getFrom().toString()+"_"+edge.getTo().toString() , edge.getFrom().toString(), edge.getTo().toString(), true);
            if(edge.isInverted()){
                Sprite inverterEdge = spriteManager.addSprite("invertedEdge_"+graphEdge.getId());
                inverterEdge.setAttribute("ui.class", "invertedEdge");
                inverterEdge.attachToEdge(edge.getFrom().toString()+"_"+edge.getTo().toString());
                inverterEdge.setPosition(0.5);
            }
        });

        if(swingUI) {
            graph.setAttribute("ui.quality");
            graph.setAttribute("ui.antialias");
        }
        graph.setAttribute("ui.stylesheet", graphStyle()+edgeStyle()+inputStyle()+outputStyle()+gateStyle()+nodeStyle()+spriteStyle());
        return graph;
    }

    public Scene getScene(Graph graph, boolean hierarchical){

        FxViewer viewer = new FxViewer(graph, FxViewer.ThreadingModel.GRAPH_IN_GUI_THREAD);

        //graph.setAttribute("ui.antialias");
        //graph.setAttribute("ui.quality");
        // https://graphstream-project.org/doc/Advanced-Concepts/GraphStream-CSS-Reference/
        graph.setAttribute("ui.stylesheet", graphStyle()+edgeStyle()+inputStyle()+outputStyle()+gateStyle()+nodeStyle()+spriteStyle());

        if(!hierarchical) {
            viewer.enableAutoLayout();
        }
        FxViewPanel panel = (FxViewPanel)viewer.addDefaultView(false, new FxGraphRenderer());

        return new Scene(panel, initialScreenSize[0], initialScreenSize[1]);
    }

}
