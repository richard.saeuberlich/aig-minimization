package gui;

public enum GraphStreamUI {

    SWING("swing"),     // https://github.com/graphstream/gs-ui-swing
    JAVAFX("javafx");   // https://github.com/graphstream/gs-ui-javafx

    private final String packageName;

    private GraphStreamUI(String packageName){
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }
}
