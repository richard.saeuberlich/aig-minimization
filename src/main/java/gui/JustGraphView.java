package gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import minimizer.AbstractAigerGraph;
import minimizer.AigerGraph;
import minimizer.Converter;
import org.graphstream.graph.Graph;

import java.nio.file.Path;
import java.nio.file.Paths;

public class JustGraphView extends Application {

    final String appName = "AIGER Minimizer";
    final boolean hierarchical = true;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene graphScene = getGraph();

        primaryStage.setTitle(appName);
        primaryStage.setScene(graphScene);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });

        primaryStage.show();
    }

    private Scene getGraph() {
        Path path_ascii = Paths.get("C:/Users/SimpleJack/Desktop/aiger-set-ascii/aig_1.aag");
        Converter converter = new Converter();
        AigerGraph aigerGraph = converter.convertAigerFile(path_ascii.toFile());
        System.out.println("\n"+aigerGraph.getBooleanFunction()+"\n");

        AbstractAigerGraph abstractAigerGraph = new AbstractAigerGraph(aigerGraph);

        // https://graphstream-project.org/doc/Tutorials/Graph-Visualisation/
        GraphBuilder builder = new GraphBuilder();
        Graph graph = builder.build(abstractAigerGraph, false, hierarchical);
        return builder.getScene(graph, hierarchical);
    }
}
