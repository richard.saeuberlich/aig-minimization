package gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class MainController {

    @FXML
    private AnchorPane leftView;

    @FXML
    private AnchorPane mainView;

    @FXML
    private AnchorPane rightView;

    @FXML
    private Label leftStatus;

    @FXML
    private Label rightStatus;

}
