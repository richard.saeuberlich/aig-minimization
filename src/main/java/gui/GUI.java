package gui;

import api.FXMLFile;
import gui.controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class GUI extends Application {

    final String appName = "AIGER Minimizer";
    final FXMLLoader loader = new FXMLLoader();
    Stage currentStage = null;

    @Override
    public void start(Stage stage) {
        stage.setTitle(appName);
        Pane mainWindow = loadPane(FXMLFile.DESKTOP);
        Scene scene = new Scene(mainWindow);
        stage.setScene(scene);
        currentStage = stage;
        currentStage.show();
    }

    public void loadNewScene(FXMLFile file) {
        Pane mainWindow = loadPane(file);
        Scene scene = new Scene(mainWindow);
        currentStage.setScene(scene);
        currentStage.show();
    }

    private Pane loadPane(FXMLFile file) {
        URL path = MainController.class.getResource(file.toString());
        loader.setLocation(path);
        try {
            return (Pane)loader.load();
        } catch (IOException exc) {
            throw new IllegalStateException("Couldn't load FXML file for creating Pane", exc);
        }
    }


}
