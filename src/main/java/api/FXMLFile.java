package api;

/**
 * ATTENTION:
 * 	Assumption here is, that all FXML-Files lives in MIRROR resource directory like gui.controller
 */
public enum FXMLFile {

    DESKTOP("mainView.fxml");

    private final String location;

    private FXMLFile(String location){
        this.location = location;
    }

    @Override
    public String toString() {
        return location;
    }
}

