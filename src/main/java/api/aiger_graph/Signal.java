package api.aiger_graph;

import java.util.List;

public interface Signal {

    boolean getSignal();

    boolean isInverted();

    /**
     * Primary inputs have no precedent signals.
     * Primary outputs have no pending signals.
     *
     * @return TRUE if, signal is primary input or output. FALSE otherwise.
     */
    boolean isPrimary();

    List<Signal> getPredecessor();

    int getID();

    /**
     * Returns boolean function for this signal and all precedents. Example: If this signal is an And-Gate with 2
     * primary inputs: 'a' and 'b', where 'b' is inverted, this method would return: (a * !b)
     *
     * @return Returns boolean expression for this and all precedent signals, if such precedents exist.
     */
    String getFunction();

}
