package api.aiger_graph;

public interface InvertSignal extends Signal {

    void invertSignal();

}
