package api.aiger_graph;

public interface Name extends ID{

    String getName();

    void setName(String name);

}
