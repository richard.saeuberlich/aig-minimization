package api.aiger_graph;

import minimizer.aiger_graph.*;

import java.util.List;

public interface AigerGraph {

    int getMaximumVariableIndex();

    int getNumberOfInputs();

    int getNumberOfLatches();

    int getNumberOfOutputs();

    int getNumberOfAndGates();

    String getComment();

    void setComment(String comment);

    List<Input> getInputs();

    List<Output> getOutputs();

    List<Latch> getLatches();

    List<AndGate> getGates();

    List<InternalSignal> getInternalSignals();

    Signal getSignalByID(int id);

    void updateInputsByID();

    void updateOutputsByID();

    void updateGatesByID();

    void updateInternalSignalsByID();

    void updateAllSignalByID();

    void clearSignalByID();

    String getBooleanFunction();
}
