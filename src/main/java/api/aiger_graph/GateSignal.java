package api.aiger_graph;

import java.util.Optional;

public interface GateSignal {

    Signal get_lhs();

    Signal get_rhs();

    Optional<Signal> getSignalByID(int id);

    boolean is_lhs_inverted();

    boolean is_rhs_inverted();

    Optional<Boolean> isInvertedByID(int id);

    void invertLHS();

    void invertRHS();

}
