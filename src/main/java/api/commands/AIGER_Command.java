package api.commands;

public enum AIGER_Command {

    AIG_AND("aigand","Conjunction of all outputs"),
    AIG_DEBUG("aigdd","Delta debugger for AIGs in AIGER format"),
    AIG_FLIP("aigflip","Flip/Negate all outputs"),
    AIG_INFO("aiginfo","Show comments of AIG"),
    AIG_FUZZ("aigfuzz (NOT FOR WINDOWS)","Fuzzer for AIGS in AIGER format"),
    AIG_JOIN("aigjoin","Join AIGs over common inputs"),
    AIG_MITER("aigmiter","Generate miter of AIGER models"),
    AIG_MOVE("aigmove","Treat non-primary outputs as primary outputs"),
    AIG_SYMBOLS("aignm","Show symbol table of AIG"),
    AIG_OR("aigor","Disjunction of all outputs"),
    AIG_RESET("aigreset","Normalize constant reset either to 0 or 1"),
    AIG_SELECT("aigselect","NO DESCRIPTION"),
    AIG_SIM("aigsim","Simulate AIG from stimulus or randomly"),
    AIG_SPLIT("aigsplit","Split outputs into separate files"),
    AIG_STRIP("aigstrip","Strip simbols from AIG"),
    AIG_TO_AIG("aigtoaig","Converts AIG formats (ascii, binary, stripped, compressed)"),
    AIG_TO_BLIF("aigtoblif","Translate AIG into BLIF"),
    AIG_TO_BTOR("aigtobtor","NO DESCRIPTION"),
    AIG_TO_CNF("aigtocnf","Translate combinational AIG into a CNF"),
    AIG_TO_DOT("aigtodot","Visualizer for AIGs using 'dot' format"),
    AIG_TO_SMV("aigtosmv","Translate sequential AIG to SMV format"),
    AIG_UNCONSTRAINT("aigunconstraint","NO DESCRIPTION"),
    AIG_UNROLL("aigunroll","Time frame expansion for bmc (previously called 'aigbmc')"),
    AND_TO_AIG("andtoaig","Translate file of AND gates into AIG"),
    BLIF_TO_AIG("bliftoaig","Translate flat BLIF model into AIG"),
    SMV_TO_AIG("smvtoaig (NOT FOR WINDOWS)","Translate flat boolean encoded SMV model into AIG"),
    SOL_TO_STIM("soltostim","Extract input vector from DIMACS solution"),
    WRAP_STIM("wrapstim","Sequential stimulus from expanded combinational stimulus");

    private final String fileName, description;

    private AIGER_Command(String fileName, String description){
        this.fileName = fileName;
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return fileName+"\n\t"+description;
    }
}
