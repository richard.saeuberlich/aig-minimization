package api.abstract_graph;

import minimizer.abstract_graph.Type;

public interface Node {

    int getID();

    Type getType();

    String getName();

    void setName(String name);

    int getDepth();

    void setDepth(int depth);

}
