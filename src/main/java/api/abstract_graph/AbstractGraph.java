package api.abstract_graph;

import minimizer.abstract_graph.Edge;
import minimizer.abstract_graph.Node;

import java.util.List;

public interface AbstractGraph {

    Node getOutputNode();

    List<Node> getInputNodes();

    List<Node> getGateNodes();

    List<Node> getAllNodes();

    boolean isInputNode(Node node);

    Edge getEdge(Node from, Node to);

    List<Edge> getEdgesFrom(Node from);

    List<Edge> getEdgesTo(Node to);

    List<Edge> getAllEdges();

    int getDepth();

    int getDepthWidth(int depth);

}
