package api.abstract_graph;

import minimizer.abstract_graph.Node;

public interface Edge {

    Node getFrom();

    Node getTo();

    boolean isInverted();

}
