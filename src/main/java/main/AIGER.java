package main;

import api.commands.AIGER_Command;
import util.FileExecuter;
import util.FileLoader;
import util.OperatingSystem;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Vector;

public class AIGER {

    private final OperatingSystem opsys = OperatingSystem.getCurrentSystem();

    public String convertBinaryToASCII(File file) {
        return convertAIGER(file, ".aag");
    }

    public String convertASCIIToBinary(File file) {
        return convertAIGER(file, ".aig");
    }

    private URI getCommandPath(AIGER_Command command) {
        switch (opsys) {
            case WINDOWS -> {
                if (command == AIGER_Command.AIG_FUZZ || command == AIGER_Command.SMV_TO_AIG) {
                    throw new IllegalArgumentException("This command ("+command.getFileName()+") doesn't work under Windows.");
                } else {
                    return FileLoader.getFile("aiger/windows/"+command.getFileName()+".exe");
                }
            }
            case LINUX -> {
                return FileLoader.getFile("aiger/linux/"+command.getFileName());
            }
            case MAC, SOLARIS -> throw new IllegalArgumentException("Not supported operating system");
        }
        return null;
    }

    private String convertAIGER(File file, String newExtension) {
        String errors;
        URI executable = getCommandPath(AIGER_Command.AIG_TO_AIG);
        try {
            List<String> command = new Vector<>();
            command.add(executable.getPath());
            command.add(file.getPath());
            String fileNameWithoutExtension = file.getName().replaceFirst("[.][^.]+$", "");
            command.add(fileNameWithoutExtension+newExtension);
            String[] result = FileExecuter.run(command);
            errors = result[1];
        } catch (IOException e) {
            e.printStackTrace();
            errors = e.getMessage();
        }
        return errors;
    }
}
