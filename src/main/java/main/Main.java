package main;

import api.abstract_graph.AbstractGraph;
import gui.GUI;
import gui.GraphBuilder;
import gui.GraphStreamUI;
import gui.JustGraphView;
import minimizer.AbstractAigerGraph;
import minimizer.Converter;
import minimizer.AigerGraph;
import minimizer.Minimizer;
import org.graphstream.graph.Graph;
import util.FileLoader;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

public class Main {

    private final static boolean printBanner = false;
    private final static boolean hierarchical = false;

    public static void main(String[] args) {
        if(printBanner) {
            printBanner();
        }
        //showGraphJavaFX(args);

        URL root = Main.class.getProtectionDomain().getCodeSource().getLocation();
        Path path_ascii = Paths.get("src","test","resources","ascii","aig_1.aag");
        Converter converter = new Converter();
        AigerGraph aigerGraph = converter.convertAigerFile(path_ascii.toFile());
        System.out.println("\n"+aigerGraph.getBooleanFunction()+"\n");

        AbstractAigerGraph abstractAigerGraph = new AbstractAigerGraph(aigerGraph);

        minimize(abstractAigerGraph);

        showGraphSwing(abstractAigerGraph);
    }

    private static void minimize(AbstractAigerGraph abstractGraph) {
        Minimizer minimizer = new Minimizer(abstractGraph);
        minimizer.start();
    }

    private static void showGraphSwing(AbstractGraph abstractGraph) {
        System.setProperty("org.graphstream.ui", GraphStreamUI.SWING.getPackageName());

        // https://graphstream-project.org/doc/Tutorials/Graph-Visualisation/
        GraphBuilder builder = new GraphBuilder();
        Graph graph = builder.build(abstractGraph, true, hierarchical);

        graph.display(!hierarchical);
    }

    private static void showGraphJavaFX(String[] args) {
        System.setProperty("org.graphstream.ui", GraphStreamUI.JAVAFX.getPackageName());

        javafx.application.Application.launch(JustGraphView.class, args);
    }

    private static void runGui(String[] args) {
        javafx.application.Application.launch(GUI.class, args);
    }

    private static void aiger() {
        //Path path_bin = Paths.get("C:/Users/SimpleJack/Desktop/aiger-set-bin");
        //convertBinaryToASCII(path_bin).forEach(System.out::println);

        Path path_ascii = Paths.get("C:/Users/SimpleJack/Desktop/aiger-set-ascii");
        //Path path_ascii = Paths.get("C:/Users/SimpleJack/Desktop/examples");
        Converter converter = new Converter();
        int i = 1;
        for(File file: path_ascii.toFile().listFiles()) {
            AigerGraph graph = converter.convertAigerFile(file);
            System.out.println("Graph "+ i++ +": "+graph.getBooleanFunction());
        }
    }

    private static void printBanner() {
        File banner = new File(FileLoader.getFile("banner.txt"));
        System.out.println();
        FileLoader.readFile(banner).forEach(System.out::println);
        System.out.println();
    }

    private static List<String> convertBinaryToASCII(Path target) {
        List<String> errors = new Vector<>();
        AIGER aiger = new AIGER();
        File targetFile = target.toFile();
        if(targetFile.exists()){
            if(targetFile.isDirectory()){
                for (File file: Objects.requireNonNull(targetFile.listFiles(), "File "+target+" does NOT exist.")){
                    errors.add(aiger.convertBinaryToASCII(file));
                }
            } else {
                errors.add(aiger.convertBinaryToASCII(targetFile));
            }
        } else {
            throw new IllegalArgumentException("Given file ("+targetFile+") does not exist!");
        }
        return errors;
    }
}
