package util;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class FileExecuter {

    /**
     * Runs the AIGer &amp; ABC executables.
     *
     * @see <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/ProcessBuilder.html">ProcessBuilder</a>
     *
     * @param command System command followed by arguments
     * @return String array with 2 elements. First is output (if no output exists, it's an empty string), second are errors (if no errors exists, it's an empty string).
     * @throws IOException
     */
    public static String[] run(List<String> command) throws IOException {
        return FileExecuter.run(command, null, null, StandardCharsets.UTF_8);
    }

    /**
     * Runs the AIGer &amp; ABC executables.
     *
     * @see <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/ProcessBuilder.html">ProcessBuilder</a>
     *
     * @param command System command followed by arguments
     * @param encoding Encoding for output stream of execution process
     * @return String array with 2 elements. First is output (if no output exists, it's an empty string), second are errors (if no errors exists, it's an empty string).
     * @throws IOException
     */
    public static String[] run(List<String> command, Charset encoding) throws IOException {
        return FileExecuter.run(command, null, null, encoding);
    }

    /**
     * Runs the AIGer &amp; ABC executables.
     *
     * @see <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/ProcessBuilder.html">ProcessBuilder</a>
     *
     * @param command System command followed by arguments
     * @param environmentParameters Environment variable settings in the format name=value
     * @return String array with 2 elements. First is output (if no output exists, it's an empty string), second are errors (if no errors exists, it's an empty string).
     * @throws IOException
     */
    public static String[] run(List<String> command, Map<String, String> environmentParameters) throws IOException {
        return FileExecuter.run(command, environmentParameters, null, StandardCharsets.UTF_8);
    }

    /**
     * Runs the AIGer &amp; ABC executables.
     *
     * @see <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/ProcessBuilder.html">ProcessBuilder</a>
     *
     * @param command System command followed by arguments
     * @param environmentParameters Environment variable settings in the format name=value
     * @param encoding Encoding for output stream of execution process
     * @return String array with 2 elements. First is output (if no output exists, it's an empty string), second are errors (if no errors exists, it's an empty string).
     * @throws IOException
     */
    public static String[] run(List<String> command, Map<String, String> environmentParameters, Charset encoding) throws IOException {
        return FileExecuter.run(command, environmentParameters, null, encoding);
    }

    /**
     * Runs the AIGer &amp; ABC executables.
     *
     * @see <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/ProcessBuilder.html">ProcessBuilder</a>
     *
     * @param command System command followed by arguments
     * @param environmentParameters Environment variable settings in the format name=value
     * @return String array with 2 elements. First is output (if no output exists, it's an empty string), second are errors (if no errors exists, it's an empty string).
     * @throws IOException
     */
    public static String[] run(List<String> command, Map<String, String> environmentParameters, File workingDir) throws IOException {
        return run(command, environmentParameters, workingDir, StandardCharsets.UTF_8);
    }

    /**
     * Runs the AIGER &amp; ABC executables and returns the output as String.
     *
     * @see <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/ProcessBuilder.html">ProcessBuilder</a>
     *
     * @param command System command followed by arguments
     * @param environmentParameters Environment variable settings in the format name=value
     * @param workingDir Working directory of the subprocess
     * @param encoding Encoding for output stream of execution process
     * @return String array with 2 elements. First is output (if no output exists, it's an empty string), second are errors (if no errors exists, it's an empty string).
     * @throws IOException
     */
    public static String[] run(List<String> command, Map<String, String> environmentParameters, File workingDir, Charset encoding) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        if (environmentParameters!=null) {
            environmentParameters.keySet().forEach(key -> processBuilder.environment().put(key, environmentParameters.get(key)));
        }
        if (workingDir != null) {
            processBuilder.directory(workingDir);
        }
        Process process = processBuilder.start();
        ProcessHandle.Info test = process.info();
        String[] result = new String[2];
        result[0] = getStringOutput(process, encoding);
        result[1] = getStringErrors(process, encoding);
        return result;
    }

    private static String getStreamAsString(InputStream stream, Charset encoding) {
        StringBuilder builder = new StringBuilder();
        InputStreamReader is = new InputStreamReader(stream, encoding);
        BufferedReader reader = new BufferedReader(is);
        reader.lines().forEach(line -> builder.append(line).append(System.lineSeparator()));
        return builder.toString();
    }

    private static String getStringErrors(Process process, Charset encoding) {
        return getStreamAsString(process.getErrorStream(), encoding);
    }

    private static String getStringOutput(Process process, Charset encoding) {
        InputStream test = process.getInputStream();
        return getStreamAsString(test, encoding);
    }

}
