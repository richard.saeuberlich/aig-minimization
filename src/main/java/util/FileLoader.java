package util;

import main.Main;

import java.io.*;
import java.net.URL;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FileLoader {

    /**
     * Info: Java streams keep order of incoming data, even on parallel operations!
     *
     * @see <a href="https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/stream/Stream.html">Stream</a>
     *
     * @param file Aiger file in ASCII format
     * @return List of string. Each string is one line from aiger file
     */
    public static List<String> readFile(File file) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            return reader.lines().filter(line -> !line.isBlank()).map(String::strip).collect(Collectors.toList());
        } catch (FileNotFoundException exc) {
            throw new IllegalArgumentException(exc);
        }
    }

    public static URI getFile(String fileName) {
        try {
            URI uri = getJarURI();
            return getFileAsURI(uri, fileName);
        } catch (URISyntaxException | IOException exc) {
            throw new RuntimeException("Couldn't load necessary file.", exc);
        }
    }

    private static URI getJarURI() throws URISyntaxException {
        return Main.class.getProtectionDomain().getCodeSource().getLocation().toURI();
    }

    private static URI getFileAsURI(URI source, String fileName) throws IOException, URISyntaxException {
        File location = new File(source);
        URI fileURI;

        if(location.isDirectory()) {
            // If not a JAR, just return the path on disk
            ClassLoader classLoader = Main.class.getClassLoader();
            URL resource = classLoader.getResource(fileName);
            if (resource == null) {
                throw new IllegalArgumentException("File ("+fileName+") not found!");
            } else {
                fileURI = resource.toURI();
            }
        } else {
            // If a JAR, extract file from JAR
            try (ZipFile zipFile = new ZipFile(location)) {
                fileURI = extractFileFromJAR(zipFile, fileName);
            }
        }
        return (fileURI);
    }

    /**
     * Extract a given file from JAR & write it to temporary file, which will be deleted after execution.
     *
     * @param zipFile JAR which holds target file
     * @param fileName File which is going to be unpacked
     * @return URI path of temporary file
     * @throws IOException
     */
    private static URI extractFileFromJAR(ZipFile zipFile, String fileName) throws IOException {
        File tempFile = File.createTempFile(fileName, Long.toString(System.currentTimeMillis()));
        tempFile.deleteOnExit();
        ZipEntry entry = zipFile.getEntry(fileName);

        if(entry == null) {
            throw new FileNotFoundException("File ("+fileName+") in archive "+zipFile.getName()+" not found!");
        } else {
            InputStream zipStream = zipFile.getInputStream(entry);
            OutputStream fileStream = null;
            try {
                final byte[] buf;
                int i;

                fileStream = new FileOutputStream(tempFile);
                buf = new byte[1024];
                i = 0;

                while ((i = zipStream.read(buf)) != -1) {
                    fileStream.write(buf, 0, i);
                }
            } finally {
                if (zipStream != null) {
                    zipStream.close();
                }
                if (fileStream != null) {
                    fileStream.close();
                }
            }
            return (tempFile.toURI());
        }
    }
}
