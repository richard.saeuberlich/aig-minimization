package util;

public enum OperatingSystem {

    WINDOWS, MAC, LINUX, SOLARIS;

    public static OperatingSystem getCurrentSystem() {

        String systemIdentifier = System.getProperty("os.name").toLowerCase();

        if(systemIdentifier.contains("win")) {
            return WINDOWS;
        } else if (systemIdentifier.contains("mac")) {
            return MAC;
        } else if (systemIdentifier.contains("nix") || systemIdentifier.contains("nux") || systemIdentifier.indexOf("aix") > 0) {
            return LINUX;
        } else if (systemIdentifier.contains("sunos")) {
            return SOLARIS;
        } else {
            throw new IllegalStateException("Unknown operating system");
        }
    }
}