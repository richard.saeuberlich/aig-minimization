package util;

import api.aiger_graph.Signal;

/**
 * Simple Signal
 */
public record SignalSearchResult(Signal signal, boolean inverted) { }
