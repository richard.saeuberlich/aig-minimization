# AIG Minimization

## 3rd Party Libraries

#### JavaFx(v.16.0.0):
  - Homepage: https://openjfx.io/
  - GitHub: https://github.com/openjdk/jfx
  
#### GraphStream(v.2.0):
  - Homepage: https://graphstream-project.org/
      - DOCs: https://graphstream-project.org/doc/
  - GitHub: https://github.com/graphstream

### Executables

#### ABC:
  - Homepage: https://people.eecs.berkeley.edu/~alanmi/abc/
  - GitHub: https://github.com/berkeley-abc/abc
  - Windows Builds: https://ci.appveyor.com/project/berkeley-abc/abc/build/artifacts
    
#### AIGER:
  - Homepage: http://fmv.jku.at/aiger/
  - GitHub: http://github.com/arminbiere/aiger

## Requirements

- Java 17 or above
    - [Adopt OpenJDK](https://adoptopenjdk.net/)
  
